import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ChatServer {
	public static void main (String [] args) {
		/*
		 * ������� ��������, ������� �������� ���� �������. ���� ���� �� ���������� ��� ��������
		 * , �� ��������������� ���� �� ���������.
		 */
		final int port = args.length > 1 ? Integer.parseInt(args[0]) : 8735;
		System.out.println("\n1: Create server connection\n" +
				"2: Create connection to a client\n" +
				"3: Select a client\n" +
				"4: Send a message\n" +
				"5: Read the messge\n" +
				"6: print information\n"+
				"7: disconnect the client\n"+
				"8: close the server connection\n"+
				"9: Exit :)\n");
		ChatServerConnection <ServerLink> server = new ChatServerConnection<ServerLink>(port);
		/*
		 * ��� ��� � ��, ������� �����
		 */
		server.setServerLinkCreator(new ServerLinkCreator<ServerLink>(){
			public ServerLink create(Socket socket) throws IOException
			{
				return new ServerLink(socket);
			}
		});
		/*
		 * link �������� ������ �� ������� ������
		 */
		ServerLink link = null;
		/*
		 * ������� ����������� ����� do while
		 */
		boolean ok = true;
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		do{
			/*
			 * choice �������� �������� �� 1 �� 9, �.�. �� ��������, ������� �� ������ ���
			 * ������ ������� ������
			 */
			System.out.println("\nYour choice\n"); 
			/*System.out.println("vvedite");
			String str = scanner.nextLine();
			System.out.println(str);*/
			int choice = scanner.nextInt();
			/*
			 * 
			 */
			switch(choice){
			/*
			 * ������� ������
			 */
			case 1:
				if (server.connect())
					System.out.println("Server create");
				else
					System.out.println("Server doesn't create");
				
			break;
			/*
			 * ������� ������� ��� �������
			 */
			case 2:
				
				if (server.acceptConnection().isConnected()){
					System.out.println("Client conected");
				} else {
					System.out.println("Client didn't conected");
				}
			break;
			/*
			 * �������� �������� �������
			 */
			case 3:
				/*
				 * �������� ��������� ������ �������
				 */
				int index = scanner.nextInt();
				/*
				 * �������� ������ �� ������, �.�. ������ ��� �������
				 */
				link = server.getLink(index);
				if (link.isConnected()){
					System.out.println("Client checked");
				} else{					
					 System.out.println("Client didn't checked");
				}
			break;
			/*
			 * �������� ��������� �������
			 */
			case 4:
				/*
				 * �������� ������ �� ������� ������
				 */
				if(link != null){
					System.out.println("Enter the message");
					/*
					 * ��������� ��������� ���������
					 */					
					String msg = scanner.next();
					try{
						 if (link.send(msg) >0 ) {
							 System.out.println("Message sent");
						 }
					}catch (Exception e){
						System.out.println("Fail to send the message ):");
					}
				} else{
					System.out.println("Choose the client");
				}
			break;
			/*
			 * ������� ��������� �� �������
			 */
			case 5:
				if(link != null){
					/*
					 * �������� ���� �� �������� ����������
					 */
					if(link.hasInput() > 0){
						try {
							String msg = new String(link.readInput());
							System.out.println(msg);
						} catch(Exception e){
							
						}
					} else {
						System.out.println("No message");
					}
				} else{
					System.out.println("Choose the client");
				}
			break;
			/*
			 * ������� ���������� � ������� ������� �������
			 */
			case 6:
				System.out.println("This is the server");
				if (server.getSocket() != null){
					System.out.println("Server socket" + server.getSocket());
				}
				
				if (link != null && link.getSocket() != null){
					System.out.println("\n\nClient IP" + link.getSocket().getInetAddress().getHostAddress());
				}
			break;
			/*
			 * ��������� ������� ��� �������� �������
			 */
			case 7 :
				if (link != null){
					server.disconnectLink(link);
					if(!link.isConnected()) System.out.println("Client disconnected");
					link = null;
				} else System.out.println("Choose the client");
			break;
			/*
			 * ��������� ������
			 */
			case 8:
				server.disconnect();
			break;
			/*
			 * ����� �� ����������
			 */
			case 9:
				ok = false;
			break;
			}			
		} while (ok);
		/*
		 * ����� ������ �� ������ ���� ����� ������ ����� 8
		 */
		server.disconnect();
	}
}
