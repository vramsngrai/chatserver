

import java.io.IOException;
import java.net.Socket;	


public interface ServerLinkCreator<E extends ServerLink> {
	public E create(Socket socket) throws IOException; // Create Link
}
