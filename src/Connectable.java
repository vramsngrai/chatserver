
import java.net.*;

/*
 * представляет соединение клиент/сервер
 */
public interface Connectable<E> {
	public boolean connect(); //return true if user connected
	public boolean disconnect();// return true if user disconnected
	public E getSocket(); //Socket used for the communicates
	public InetAddress getLocalIp();//get ip of the client
	public InetAddress getRemoteIp();//get ip of the server
	public int getLocalPort(); // Get port for the communication(client)
	public int getRemotePort(); // get port for the communication (server)
	public boolean isServer();
}
